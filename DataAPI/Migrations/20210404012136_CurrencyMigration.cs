﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DataAPI.Migrations
{
    public partial class CurrencyMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "_TarihDate",
                columns: table => new
                {
                    TarihDateID = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Tarih = table.Column<string>(type: "text", nullable: true),
                    Date = table.Column<string>(type: "text", nullable: true),
                    Bulten_No = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__TarihDate", x => x.TarihDateID);
                });

            migrationBuilder.CreateTable(
                name: "_CurrencyModel",
                columns: table => new
                {
                    ID = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Unit = table.Column<string>(type: "text", nullable: true),
                    isim = table.Column<string>(type: "text", nullable: true),
                    CurrencyName = table.Column<string>(type: "text", nullable: true),
                    ForexBuying = table.Column<string>(type: "text", nullable: true),
                    ForexSelling = table.Column<string>(type: "text", nullable: true),
                    BanknoteBuying = table.Column<string>(type: "text", nullable: true),
                    BanknoteSelling = table.Column<string>(type: "text", nullable: true),
                    CrossRateUSD = table.Column<string>(type: "text", nullable: true),
                    CrossRateOther = table.Column<string>(type: "text", nullable: true),
                    CrossOrder = table.Column<string>(type: "text", nullable: true),
                    Kod = table.Column<string>(type: "text", nullable: true),
                    CurrencyCode = table.Column<string>(type: "text", nullable: true),
                    TarihDateID = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__CurrencyModel", x => x.ID);
                    table.ForeignKey(
                        name: "FK__CurrencyModel__TarihDate_TarihDateID",
                        column: x => x.TarihDateID,
                        principalTable: "_TarihDate",
                        principalColumn: "TarihDateID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX__CurrencyModel_TarihDateID",
                table: "_CurrencyModel",
                column: "TarihDateID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "_CurrencyModel");

            migrationBuilder.DropTable(
                name: "_TarihDate");
        }
    }
}
