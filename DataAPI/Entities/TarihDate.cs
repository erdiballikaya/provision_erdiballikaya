﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAPI.Entities
{
    public class TarihDate
    {
        public int TarihDateID { get; set; }
        public string Tarih { get; set; }

        public string Date { get; set; }

        public string Bulten_No { get; set; }

        public virtual List<CurrencyModel> Currencies { get; set; }

    }
}
