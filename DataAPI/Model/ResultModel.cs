﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAPI.Model
{
    public class ResultModel
    {
        public string Content { get; set; }
        public int Status { get; set; }
    }

    public class ResultDateAndCurrency
    {
        public string Value { get; set; }
        public string Date { get; set; }
    }
}
