﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAPI.Actions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DataAPI.Controller
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CurrencyController : ControllerBase
    {
        private readonly ICurrencyManagement currency;

        public CurrencyController(ICurrencyManagement _currency)
        {
            currency = _currency;
        }
        //BussinessAPI den gelen istek doğrultusna göre veritabanına ekleme yapar.
        [HttpGet]
        public IActionResult Add()
        {
            return Ok(currency.Add());
        }
        //BussinessAPI den gelen istek doğrultusna göre döviz kuruna göre 20 günün verisini döner 
        [HttpGet]
        public IActionResult GetByCurrencyCode(string code)
        {
            return Ok(currency.GetByCurrencyCode(code));
        }
    }
}
