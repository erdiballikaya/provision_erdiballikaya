﻿using DataAPI.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace DataAPI.Actions
{
    public interface ICurrencyManagement
    {
        ResultModel Add();
        List<ResultDateAndCurrency> GetByCurrencyCode(string code);
    }
}
