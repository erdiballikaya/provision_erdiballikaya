﻿using Microsoft.AspNetCore.Mvc;
using System; 
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using DataAPI.Model;
using DataAPI.Entities;
using RestSharp;
namespace DataAPI.Actions
{
    public class CurrencyManagement : ICurrencyManagement
    {
        private readonly PostgreDbContext postgreDbContext;

        public CurrencyManagement(PostgreDbContext _postgreDbContext)
        {
            postgreDbContext = _postgreDbContext;
        }
        
        public ResultModel Add()
        {   
            
            ClearDataBase(); // Veritabını temizleyen foksiyonu çağırır.
            var days =  GetLastTwoMonthDays(); // Son 20 günün Date bilgisini döner 1 gün eksik döner. (Bulunulan günün döviz bilgilerinin daha mevcut olmaması ihtimaline karşı)

            foreach (DateTime date in days)
            {
                string yearMonth = date.ToString("yyyyMM"); //Date bilgisini Tcmb Url ine göre düzenler.
                string dayMonthYear = date.ToString("ddMMyyyy");

                string url = "https://www.tcmb.gov.tr/kurlar/" + yearMonth + "/" + dayMonthYear + ".xml";

                string day = CultureInfo.GetCultureInfo("en-US").DateTimeFormat.DayNames[(int)date.DayOfWeek].ToString();

                if (day != "Saturday" && day != "Sunday" && day != "Cumartesi" && day != "Pazar" && date.Date != DateTime.Today) //Haftasonu kontrolü
                {
                    var save = SaveCurrency(url);

                    if (save == false)
                    {
                        return new ResultModel { Content = "Verilerin aktarımı sırasında hata tarih: " + date + " URL: " + url, Status = 200 };
                    }
                }
            }

            return new ResultModel { Content = "Aktarım Tamamlandı", Status = 200 };
                                  

        }                                

        public List<ResultDateAndCurrency> GetByCurrencyCode (string code) 
        {
            var res = postgreDbContext._CurrencyModel.Join(
                postgreDbContext._TarihDate,
                CC => CC.TarihDateID,
                TD => TD.TarihDateID,
                (CC, TD) => new
                {
                    CurrencyCode = CC.CurrencyCode,
                    Currency = CC.BanknoteSelling,
                    TarihDate = TD.Date
                }
            ).Where(x => x.CurrencyCode == code).ToList(); //Döviz Kur Koduna göre son 20 günün BanknoteSelling bilgilerini döner

            List<ResultDateAndCurrency> resultList = new List<ResultDateAndCurrency>();

            foreach(var item in res)
            {
                ResultDateAndCurrency temp = new ResultDateAndCurrency(); //Result tipini doldurur.
                temp.Value = item.Currency;
                temp.Date = item.TarihDate;

                resultList.Add(temp);
            }

            return resultList;

        }

        public List<DateTime> GetLastTwoMonthDays() //son 20 günü döner
        {
            List<DateTime> allDates = new List<DateTime>();

            DateTime twoMonthsAgo = new DateTime(DateTime.Now.Year, DateTime.Now.Month - 2, DateTime.Now.Day);

            for (DateTime date = DateTime.Now; twoMonthsAgo <= date; twoMonthsAgo = twoMonthsAgo.AddDays(1))
            {
                allDates.Add(twoMonthsAgo);
            }

            return allDates;
        }

        public bool SaveCurrency (string url)
        {
            

            XmlDocument document = new XmlDocument();
            document.Load(url);

            XmlSerializer serializer = new XmlSerializer(typeof(Tarih_Date));

            Tarih_Date curr = new Tarih_Date();

            Stream stream = new MemoryStream(Encoding.UTF8.GetBytes(document.InnerXml));

            curr = (Tarih_Date)serializer.Deserialize(stream);

            var res = 0;

            foreach(Tarih_DateCurrency currency in curr.Currency)
            {
                TarihDate _tarihDate = new TarihDate();
                _tarihDate.Bulten_No = curr.Bulten_No;
                _tarihDate.Date = curr.Date;
                _tarihDate.Tarih = curr.Tarih;
                

                CurrencyModel _currency = new CurrencyModel();
                _currency.isim = currency.Isim;
                _currency.Kod = currency.Kod;
                _currency.Unit = currency.Unit;
                _currency.ForexBuying = currency.ForexBuying;
                _currency.ForexSelling = currency.ForexSelling;
                _currency.CurrencyName = currency.CurrencyName;
                _currency.CrossRateUSD = currency.CrossRateUSD;
                _currency.CrossRateOther = currency.CrossRateOther;
                _currency.CrossOrder = currency.CrossOrder;
                _currency.BanknoteSelling = currency.BanknoteSelling;
                _currency.BanknoteBuying = currency.BanknoteBuying;
                _currency.CurrencyCode = currency.CurrencyCode;
                _currency.TarihDate = _tarihDate;
                

                postgreDbContext.Add(_tarihDate);
                postgreDbContext.Add(_currency); 
                
            } 
                res = postgreDbContext.SaveChanges();          
            
            if (res == 0)
                return false;
            else
                return true;
        } //Son 20 günü veritabanına ekler.

        public void ClearDataBase()
        {   
            postgreDbContext._CurrencyModel.RemoveRange(postgreDbContext._CurrencyModel);   
            postgreDbContext._TarihDate.RemoveRange(postgreDbContext._TarihDate);   

            postgreDbContext.SaveChanges();

        } //Yani veriler ekleneceği zaman veritabanını temizler.
    }
}
