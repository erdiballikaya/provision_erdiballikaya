using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace DataAPI.Services
{
    public class DataBaseManagement
    {
        public static void MigrationInistall(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                serviceScope.ServiceProvider.GetService<PostgreDbContext>().Database.Migrate();
            }
        }

    }

}