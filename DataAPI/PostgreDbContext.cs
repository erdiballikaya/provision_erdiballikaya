﻿using DataAPI.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAPI
{
    public class PostgreDbContext : DbContext
    {
        public PostgreDbContext(DbContextOptions<PostgreDbContext> options):base(options)
        {

        }

        public virtual DbSet<TarihDate> _TarihDate { get; set; }
        public virtual DbSet<CurrencyModel> _CurrencyModel { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(PostgreDbContext).Assembly);
        }
    }
}
