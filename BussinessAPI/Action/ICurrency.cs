﻿using BussinessAPI.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BussinessAPI.Action
{
    public interface ICurrency
    {
        List<ResultDateAndCurrency> GetCurrency(string CurrencyCode);

        ResultModel AddDataBase();

        bool ClearRedis();
    }
}
