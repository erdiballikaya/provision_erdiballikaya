﻿using BussinessAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BussinessAPI.Action;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using RestSharp;
using Microsoft.AspNetCore.Mvc;
using StackExchange.Redis;

namespace BussinessAPI.Action
{
    public class Currency : ICurrency
    {
        string Baseurl = "http://provision_dataapi_1/"; //DataApi bağlantı adresi
        private readonly IDatabase _database;

        public Currency(IDatabase database)
        {
            _database = database;
        }

        public List<ResultDateAndCurrency> GetCurrency(string CurrencyCode)
        {
            var key = Get(CurrencyCode); //Get ile redis' vu koda ait veri var mı diye sorar.

            if(key == "null" || key == null)
            {
                var client = new RestClient(Baseurl + "api/Currency/GetByCurrencyCode?code=" + CurrencyCode); // Eğer redis de veri yoksa DataAPI ye gider.

                client.Timeout = -1;

                var request = new RestRequest(Method.GET);

                request.AddHeader("Content-Type", "application/json");

                IRestResponse response = client.Execute(request);

                if(response.Content != "[]" || response.Content == "")
                {
                    Console.WriteLine(response.Content);

                    var res = JsonConvert.DeserializeObject<List<ResultDateAndCurrency>>(response.Content);

                    var resStringForRedis = JsonConvert.SerializeObject(res);

                    var sendKeyValue = KeyValuePair.Create(key: CurrencyCode, value: resStringForRedis); // Redis için key value hazırlar key=KOD Value= 20 günlük veri(string)

                    Post(sendKeyValue); //Veriyi Redise gönderir.

                    return res;
                }
                else
                {
                    return new List<ResultDateAndCurrency>(){};
                }

            }
            else
            {
                var res = JsonConvert.DeserializeObject<List<ResultDateAndCurrency>>(key); //Redis den gelen bilgiyi objeye çeverirek döner.

                return res;
            }
        }

        public ResultModel AddDataBase()
        {
            var client = new RestClient(Baseurl + "api/Currency/Add"); //DataApi ye istek atarak veritabanına ekleme yapar.

            client.Timeout = -1;

            var request = new RestRequest(Method.GET);

            request.AddHeader("Content-Type", "application/json");

            IRestResponse response = client.Execute(request);

            var isRedisClear = ClearRedis();

            if(response.StatusCode.ToString() == "OK" && isRedisClear == true) //Redis sunucusu temizlendimi diye kontrol eder.
            {
                if(isRedisClear == false)
                {
                    return new ResultModel { Content = "Yeni veriler aktarıldı ve ancak Cache temizlenemedi.", Status = 200 };
                }

                return new ResultModel { Content = "Yeni veriler aktarıldı ve Cache temizlendi", Status = 200 };

            }
            else
                return new ResultModel { Content = "Sunucu Meşgul Tekrar Deneyin", Status = 404 };

        }

        // GET api/<CacheController>/5
        public string Get([FromQuery] string key)
        {
            return _database.StringGet(key);
        } //Redis sunucusunu test etmek için Get metodu. (Postman ve talendAPI ile kullanılabilir.)

        // POST api/<CacheController>
        public void Post([FromBody] KeyValuePair<string, string> KeyValue)
        {
            _database.StringSet(KeyValue.Key, KeyValue.Value);
        } //Redis sunucusunu test etmek için Set metodu. (Postman ve TalendAPI ile kullanılabilir.)


        public bool ClearRedis()
        {
            var sendKeyValue = KeyValuePair.Create(key: "CurrencyCode", value: "resStringForRedis");
            Post(sendKeyValue);
            var options = ConfigurationOptions.Parse("provision_redis_1:6379");
            options.AllowAdmin = true;

            var redis = ConnectionMultiplexer.Connect(options);

            var endpoints = redis.GetEndPoints();
            var server = redis.GetServer(endpoints[0]);
            server.FlushAllDatabases();

            return true;
        } //Redisi Temizler.

    }
}
