﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BussinessAPI.Models
{
    public class ResultDateAndCurrency
    {
        public string value { get; set; }
        public string date { get; set; }
    }

    public class ResultModel
    {
        public string Content { get; set; }
        public int Status { get; set; }
    }
}
