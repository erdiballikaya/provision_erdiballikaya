﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using StackExchange.Redis;
using BussinessAPI.Action;
using Microsoft.AspNetCore.Cors;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BussinessAPI.Controllers
{   [EnableCors("DCors")] //CORS Hatası için
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CacheController : ControllerBase
    {
        
        private readonly ICurrency currency;
        private readonly IDatabase _database;

        public CacheController(IDatabase database, ICurrency _currency)
        {
            currency = _currency;
            _database = database;
        }


        //Girilen döviz kuruna ait son 20 günün BanknoteSelling değerini döner, önce redise sorar eğer redisde bu koda ait veri yoksa veritabanından verileri getirir ve redis e ekler.
        [HttpGet]
        public IActionResult GetCurrency(string CurrencyCode)
        {
           return Ok(currency.GetCurrency(CurrencyCode));
        }

        //Tüm verileri silerek son 20 günün tüm verilerini veritabanına kaydeder. Verileri redis de döviz koduna göre tuttuğum için yeni gelen verilerin günleri başka olduğundan redisi temizler
        [HttpGet]
        public IActionResult AddDataBase()
        {
            return Ok(currency.AddDataBase());
        }

        //redisi temizleyen fonk.
        [HttpGet]
        public IActionResult ClearRedis()
        {
            return Ok(currency.ClearRedis());
        }

    }
}
